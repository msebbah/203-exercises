import numpy as np


def bsm_call_montecarlo(S0, K, T, r, sigma, I):
    ''' Valuation of European call option in BSM model.
    Monte-Carlo Method.
    
    Parameters
    ==========
    S0 : float
        initial stock/index level
    K : float
        strike price
    T : float
        maturity date (in year fractions)
    r : float
        constant risk-free short rate
    sigma : float
        volatility factor in diffusion term
    I : integer
        number of simulations
    
    Returns
    =======
    value : float
        present value of the European call option
    '''
    pass


# bsm_call_value(S0=100., K=105., T=10., r=0.01, sigma=0.15, I=1000000)
# should be closed to 20.88